/* 
Exercise 1
input:  1. Tiền lương 1 ngày 
        2. Số ngày làm

step:   1. Khởi tạo 3 biến salary, dayQty và totalSalary
        2. Gán số tiền lương 100.000 vào biến salary
        3. Gán số ngày làm vào biến dayQty
        4. Tổng số lương được tính bằng công thức: totalSalary = salary*dayQty
        5. In kết quả

output: Tổng số tiền lương theo ngày làm
*/
var salary = 100000;
var dayQty = 25;
var totalSalary = salary * dayQty;
console.log(
  "Exercise 1: Tổng số tiền lương của " + dayQty + " ngày = " + totalSalary
);
/* 
Exercise 2
input:  5 số thực

step:   1. Khởi tạo 6 biến number1, number2, number3, number4, number5 và avgNumber
        2. Gán giá trị số thực vào 5 biến number1, number2, number3, number4, number5
        3. Giá trị trung bình được tính bằng công thức: avgNumber = (number1 + number2 + number3 + number4 + number5)/5
        4. In kết quả

output: Giá trị trung bình của 5 số thực
*/
var number1 = 2.5;
var number2 = 3.6;
var number3 = 4.7;
var number4 = 5.6;
var number5 = 6.9;
var avgNumber = (number1 + number2 + number3 + number4 + number5) / 5;
console.log(
  "Exercise 2: Giá trị trung bình của 5 số " +
    number1 +
    "," +
    number2 +
    "," +
    number3 +
    "," +
    number4 +
    "," +
    number5 +
    " = " +
    avgNumber
);
/* 
Exercise 3
input:  1. Tỉ giá VND/USD hiện nay
        2. Số tiền USD cần quy đổi

step:   1. Khởi tạo 2 biến exchangeRate, moneyQty và result
        2. Gán giá trị tiền quy đổi 23.500 vào biến exchangeRate
        3. Gán số lượng cần quy đổi vào biến moneyQty
        4. Số tiền sau quy đổi được tính bằng công thức: result = exchangeRate*moneyQty
        5. In kết quả

output: Số tiền sau quy đổi
*/
var exchangeRate = 23500;
var moneyQty = 2;
var result = exchangeRate * moneyQty;
console.log(
  "Exercise 3: Số tiền sau quy đổi " + moneyQty + " USD = " + result + " VND"
);
/* 
Exercise 4
input:  1. Chiều dài của HCN
        2. Chiều rộng của HCN

step:   1. Khởi tạo 4 biến: length, width, perimeter và area
        2. Gán giá trị chiều dài vào biến length
        3. Gán giá trị chiều rộng vào biến width
        4. Chu vi HCN được tính bằng công thức: perimeter = 2*(length+width)
        5. Diện tích HCN được tính bằng công thức: area = length*width
        6. In kết quả

output: 1. Chu vi của HCN
        2. Diện tích của HCN
*/
var length = 20;
var width = 10;
var perimeter = 2 * (length + width);
var area = length * width;
console.log(
  "Exercise 4:" +
    "\n" +
    "Với chiều dài = " +
    length +
    "\n" +
    "Chiều rộng = " +
    width +
    "\n" +
    "Chu vi HCN = " +
    perimeter +
    "\n" +
    "Diện tích HCN = " +
    area
);
/* 
Exercise 5
input:  Số nguyên dương number có 2 chữ số

step:   1. Khởi tạo biến number, unit, ten, sum
        2. Gán giá trị cho number
        3. Tách số hàng đơn vị theo công thức: unit = number%10
        4. Tách số hàng chục theo công thức: ten = number/10
        5. Tổng 2 ký số được tính bằng công thức: sum = unit + ten

output: Tổng của 2 ký số
*/
var number = 47;
var unit = number % 10;
var ten = Math.floor(number / 10);
var sum = unit + ten;
console.log("Exercise 5: Tổng 2 ký số của " + number + " = " + sum);
